# Changelog
All notable changes to this project will be documented in this file.

## [0.0.1] - 2022-07-29
### Added
- New example for a custom package manager

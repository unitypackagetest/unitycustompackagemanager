using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;

public class ExampleTool : EditorWindow
{
    public string[] options = new string[] { "v1.0.0", "v1.0.2" };
    public int index = 0;
    private AddRequest addRequest;
    private RemoveRequest removeRequest;


    // Add menu named "My Window" to the Window menu
    [MenuItem("Niantic/PackageManager")]
    private static void Init()
    {
        // Get existing open window or if none, make a new one:
        ExampleTool window = (ExampleTool)EditorWindow.GetWindow(typeof(ExampleTool));
        window.Show();
    }

    private void OnGUI()
    {
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 15;
        GUILayout.Label("NIANTIC", style);

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        var textStyle = GUI.skin.GetStyle("label");
        textStyle.fontSize = 12;
        GUILayout.Label("Package test from a git repo", textStyle);
        GUILayout.Space(10);
        index = EditorGUILayout.Popup(index, options);
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        if (GUILayout.Button("ADD PACKAGE"))
        {
            Install();
        }

        if (GUILayout.Button("REMOVE PACKAGE"))
        {
            Uninstall();
        }
    }

    private void Install()
    {
        Debug.Log("Installing " + options[index] + " package");
        addRequest = Client.Add("https://gitlab.com/unitypackagetest/unitypackagetest.git#" + options[index]);
        EditorApplication.update += Progress;
    }

    private void Progress()
    {
        if (addRequest.IsCompleted)
        {
            if (addRequest.Status == StatusCode.Success)
                Debug.Log("Installed: " + addRequest.Result.packageId);
            else if (addRequest.Status >= StatusCode.Failure)
                Debug.Log(addRequest.Error.message);

            EditorApplication.update -= Progress;
        }
    }

    private void Uninstall()
    {
        Debug.Log("Removing " + options[index] + " package");
        removeRequest = Client.Remove("com.niantic.custompackage");
        EditorApplication.update += UninstallProgress;
    }

    private void UninstallProgress()
    {
        if (removeRequest.IsCompleted)
        {
            if (removeRequest.Status == StatusCode.Success)
                Debug.Log("uninstalled: " + removeRequest.PackageIdOrName);
            else if (removeRequest.Status >= StatusCode.Failure)
                Debug.Log(removeRequest.Error.message);

            EditorApplication.update -= UninstallProgress;
        }
    }
}
